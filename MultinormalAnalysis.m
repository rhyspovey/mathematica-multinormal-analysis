(* ::Package:: *)

(* ::Title:: *)
(*MultinormalAnalysis*)


(* ::Text:: *)
(*Rhys G. Povey <rhyspovey@gmail.com>*)


(* ::Text:: *)
(*TODO: Add warning if \[CapitalSigma]start < 0 (data unc likely overestimated.)*)


(* ::Section:: *)
(*Front End*)


BeginPackage["MultinormalAnalysis`"];


VoigtOrder::usage="VoigtOrder[\!\(\*
StyleBox[\"d\",\nFontSlant->\"Italic\"]\)] generates the Voigt ordering of elements for a symmetric \!\(\*
StyleBox[\"d\",\nFontSlant->\"Italic\"]\)\[Times]\!\(\*
StyleBox[\"d\",\nFontSlant->\"Italic\"]\) matrix.";


BlockDiagonalMatrix::usage="BlockDiagonalMatrix[{\!\(\*SubscriptBox[\(M\), \(1\)]\),\!\(\*SubscriptBox[\(M\), \(2\)]\),\[Ellipsis]}] assembles a block diagonal matrix from matrix inputs.";


NMultinormalAnalyze::usage="NMultinormalAnalyze[{{\!\(\*SubscriptBox[\(\[Mu]\), \(1\)]\),\!\(\*SubscriptBox[\(\[CapitalSigma]\), \(1\)]\)},\[Ellipsis]}] does general numeric analysis to determine a source multinormal distribution from a given set of independent multinormal distribution measurements. Each \!\(\*SubscriptBox[\(\[Mu]\), \(i\)]\) should be a vector of length \!\(\*
StyleBox[\"d\",\nFontSlant->\"Italic\"]\), and \!\(\*SubscriptBox[\(\[CapitalSigma]\), \(i\)]\) a \!\(\*
StyleBox[\"d\",\nFontSlant->\"Italic\"]\)\[Times]\!\(\*
StyleBox[\"d\",\nFontSlant->\"Italic\"]\) matrix.";


NNormalAnalyze::usage="NNormalAnalyze[{{\!\(\*SubscriptBox[\(\[Mu]\), \(1\)]\),\!\(\*SubscriptBox[\(\[Sigma]\), \(1\)]\)},{\!\(\*SubscriptBox[\(\[Mu]\), \(2\)]\),\!\(\*SubscriptBox[\(\[Sigma]\), \(2\)]\)},\[Ellipsis]}] does generic numeric analysis by calling \!\(\*
StyleBox[\"d\",\nFontSlant->\"Italic\"]\)=1 NMultiNormalAnalyze.";


NMultinormalAnalyze::depth="Input data elements should be {vector,matrix} pairs.";


NMultinormalAnalyze::dimension="Input data dimension not consistent.";


NMultinormalAnalyze::variance="Data variance values are larger than the variance of data means, and may be overestimated.";


NMultinormalAnalyze::biased="\[CapitalSigma] estimator is biased.";


NMultinormalAnalyze::unbias="Guessing unbiased \[CapitalSigma].";


NMultinormalAnalyze::CramerRao="Covariance is Cram\[EAcute]r-Rao lower bound.";


NMultinormalAnalyze::output="Output `1` is unrecognized.";


SeparateMultinormal::usage="SeparateMultinormal[MultinormalDistribution[\[Ellipsis]]] separates a multinormal distribution into a product of normal distributions, discarding any correlations.";


SeparateMultinormal::covariance="Non-zero covariance discarded.";


NIndepNormalAnalyze::usage="NIndepNormalAnalyze[{{\!\(\*SubscriptBox[\(\[Mu]\), \(1\)]\),\!\(\*SubscriptBox[\(\[CapitalSigma]\), \(1\)]\)},\[Ellipsis]}] does NMultinormalAnalyze discaring covariances within each independent measurement (keeping just variances), such that the analysis becomes a set of NNormalAnalyze. Each \!\(\*SubscriptBox[\(\[Mu]\), \(i\)]\) should be a vector of length \!\(\*
StyleBox[\"d\",\nFontSlant->\"Italic\"]\), and \!\(\*SubscriptBox[\(\[CapitalSigma]\), \(i\)]\) a \!\(\*
StyleBox[\"d\",\nFontSlant->\"Italic\"]\)\[Times]\!\(\*
StyleBox[\"d\",\nFontSlant->\"Italic\"]\) matrix.";


(* ::Section:: *)
(*Back End*)


Begin["Private`"];


VoigtOrder[\[FormalD]_Integer]:=Module[{elements,pos,dir},
elements=Union@Flatten[Table[Sort[{\[FormalI],\[FormalJ]}],{\[FormalI],\[FormalD]},{\[FormalJ],\[FormalD]}],1];

pos={0,0};
dir={1,1};

Reap[
While[Length[elements]>0,
If[Not@MemberQ[elements,pos+dir],dir=Switch[dir,{1,1},{-1,0},{-1,0},{0,-1},{0,-1},{1,1}]];
pos=pos+dir;
Sow[pos];
elements=DeleteCases[elements,pos];
];
][[2,1]]

];


BlockDiagonalMatrix[inputs_List]:=Module[{dims,cumulativedims,inputlist},
cumulativedims={0,0};
Normal@SparseArray[
Reap[
Do[
dims=Dimensions[in][[;;2]];
Sow[Table[{i,j},{i,1+cumulativedims[[1]],dims[[1]]+cumulativedims[[1]]},{j,1+cumulativedims[[2]],dims[[2]]+cumulativedims[[2]]}]->in];
cumulativedims=cumulativedims+dims;,
{in,inputs}]
][[2,1]]
]
];


NMultinormalAnalyze[data_List,options:OptionsPattern[]]:=Module[
{d,n,voigtorder,\[Mu],\[Mu]vector,\[CapitalSigma],\[CapitalSigma]matrix,\[CapitalSigma]list,params,paramnames,log\[ScriptCapitalL],\[CapitalSigma]0,\[Mu]guess,\[CapitalSigma]guess,guesses,nmax,\[Mu]result,\[CapitalSigma]result,\[ScriptCapitalI]\[Mu],\[ScriptCapitalI]\[CapitalSigma],\[ScriptCapitalI],CramerRao,estimates,stddevs,\[Mu]unc,\[CapitalSigma]unc},
If[Depth[data]=!=5,Message[NMultinormalAnalyze::depth];Return[$Failed]];
If[Length[#]===1,d=First[#],Message[NMultinormalAnalyze::dimension];Return[$Failed]]&[Union@Flatten@Map[Dimensions,data,{2}]];
n=Length[data];
voigtorder=VoigtOrder[d];

\[Mu]vector=Table[\[Mu][\[FormalI]],{\[FormalI],d}];
\[CapitalSigma]matrix=Table[\[CapitalSigma]@@Sort[{\[FormalI],\[FormalJ]}],{\[FormalI],d},{\[FormalJ],d}];
\[CapitalSigma]list=Extract[\[CapitalSigma]matrix,voigtorder];
params=Join[\[Mu]vector,\[CapitalSigma]list];
paramnames=Join[Subscript["\[Mu]",#]&/@Range[d],Subscript[If[OptionValue["UnbiasedGuess"],"\[CapitalSigma]s","\[CapitalSigma]"],Row[#," "]]&/@voigtorder];
If[OptionValue[Verbose],Print["Parameters: ",Row[paramnames,", "]]];

(* log likelihood *)
log\[ScriptCapitalL]=-((n d)/2)Log[2\[Pi]]-1/2 Plus@@(Log@Det[\[CapitalSigma]matrix+#[[2]]]&/@data)-1/2 Plus@@((#[[1]]-\[Mu]vector).Inverse[\[CapitalSigma]matrix+#[[2]]].(#[[1]]-\[Mu]vector)&/@data);
(*If[OptionValue[Verbose],Print["Log likelihood function: ",log\[ScriptCapitalL]]];*)

(* Guess starting points based on average measurement covariance *)
\[CapitalSigma]0=Mean[data[[All,2]]];
\[Mu]guess=Mean[data[[All,1]]];
\[CapitalSigma]guess=Covariance[data[[All,1]]]-\[CapitalSigma]0;
If[Det[\[CapitalSigma]guess]<0,Message[NMultinormalAnalyze::variance];\[CapitalSigma]guess=ConstantArray[0,{d,d}]];
guesses=Join[\[Mu]guess,Extract[\[CapitalSigma]guess,voigtorder]];
If[OptionValue[Verbose],Print["\!\(\*SubscriptBox[\(\[CapitalSigma]\), \(0\)]\): ",MatrixForm[\[CapitalSigma]0]];Print["\!\(\*SubscriptBox[\(\[Mu]\), \(0\)]\): ",MatrixForm[\[Mu]guess]];Print["\!\(\*SubscriptBox[\(\[CapitalSigma]\), \(start\)]\): ",MatrixForm[\[CapitalSigma]guess]]];

(* Numeric maximization *)
nmax=If[OptionValue["Constrain\[CapitalSigma]"],
FindMaximum[{log\[ScriptCapitalL],Evaluate[Det[\[CapitalSigma]matrix]]>0},Join[{params}\[Transpose],{guesses}\[Transpose],2]],
FindMaximum[log\[ScriptCapitalL],Join[{params}\[Transpose],{guesses}\[Transpose],2]]
];
\[Mu]result=(\[Mu]vector/.nmax[[2]]);
\[CapitalSigma]result=(\[CapitalSigma]matrix/.nmax[[2]]);

(* Bias *)
If[OptionValue["UnbiasedGuess"],
Message[NMultinormalAnalyze::unbias];
If[OptionValue[Verbose],Print["Bias removal: ",MatrixForm[\[CapitalSigma]result],"\[Function]",MatrixForm[(n \[CapitalSigma]result+\[CapitalSigma]0)/(n-1)]]];
\[CapitalSigma]result=(n \[CapitalSigma]result+\[CapitalSigma]0)/(n-1),
Message[NMultinormalAnalyze::biased]
];

(* Uncertainty *)
(* Treat resulst as (d^2+3d)/2 parameter multinormal in vicinity of peak *)
estimates=Join[\[Mu]result,Extract[\[CapitalSigma]result,voigtorder]];
\[ScriptCapitalI]\[Mu]=Plus@@(Inverse[\[CapitalSigma]result+#[[2]]]&/@data);
\[ScriptCapitalI]\[CapitalSigma]=Table[
Plus@@((Inverse[\[CapitalSigma]result+#[[2]]][[ab[[1]],cd[[1]]]]Inverse[\[CapitalSigma]result+#[[2]]][[ab[[2]],cd[[2]]]]+Inverse[\[CapitalSigma]result+#[[2]]][[ab[[1]],cd[[2]]]]Inverse[\[CapitalSigma]result+#[[2]]][[ab[[2]],cd[[1]]]])&/@data)
/((1+KroneckerDelta@@ab)(1+KroneckerDelta@@cd)),
{ab,voigtorder},{cd,voigtorder}];
\[ScriptCapitalI]=BlockDiagonalMatrix[{\[ScriptCapitalI]\[Mu],\[ScriptCapitalI]\[CapitalSigma]}];
CramerRao=Inverse[\[ScriptCapitalI]];
stddevs=Sqrt[#]&/@Diagonal[CramerRao];
Message[NMultinormalAnalyze::CramerRao];
\[Mu]unc=(Sqrt[#]&/@Diagonal[CramerRao[[1;;d,1;;d]]]);
\[CapitalSigma]unc=Table[(Sqrt[#]&/@Diagonal[CramerRao[[d+1;;,d+1;;]]])[[First@FirstPosition[voigtorder,Sort[{\[FormalI],\[FormalJ]}]]]],{\[FormalI],d},{\[FormalJ],d}];

(* Diagnostics *)
If[OptionValue[Verbose]\[And]Not[OptionValue["FitDiagnostics"]],Print["Set \"FitDiagnostics\"->True for fit diagnostic plots."]];
(* Projection along each parameter axis *)
If[OptionValue["FitDiagnostics"],
Print@Column[Table[
With[{
f=Simplify[log\[ScriptCapitalL]/.Drop[nmax[[2]],{\[FormalI]}]],
range={params[[\[FormalI]]],estimates[[\[FormalI]]]-2stddevs[[\[FormalI]]],estimates[[\[FormalI]]]+2stddevs[[\[FormalI]]]},
rect=Rectangle[{estimates[[\[FormalI]]]-stddevs[[\[FormalI]]],nmax[[1]]-0.5},{estimates[[\[FormalI]]]+stddevs[[\[FormalI]]],nmax[[1]]}],
line=Line[{{estimates[[\[FormalI]]],nmax[[1]]-0.5},{estimates[[\[FormalI]]],nmax[[1]]}}],
p=paramnames[[\[FormalI]]]
},
Plot[{f,nmax[[1]],nmax[[1]]-0.5},range,
Frame->True,FrameLabel->{p,"Log\[ScriptCapitalL]"},
PlotStyle->{RGBColor[0.39215686274509803`, 0.5843137254901961, 0.9294117647058824],RGBColor[0., 0., 0.501999],RGBColor[0., 0., 0.501999]},
Prolog->{
RGBColor[0.9333333333333333, 0.9098039215686274, 0.6666666666666666],rect,
RGBColor[1., 1., 0.8784313725490196],line
},FilterRules[{options},Options[Plot]]]
],{\[FormalI],(d^2+3d)/2}],Spacings->5]
];

(* Output *)
Switch[OptionValue["Output"],
"Association",
<|
"Parameters"->paramnames,
"Estimates"->estimates,
"FisherInformation"->\[ScriptCapitalI],
"CovarianceMatrix"->CramerRao,
"\[Mu]"->\[Mu]result,"\[CapitalSigma]"->\[CapitalSigma]result,
"\[Mu]Covariance"->CramerRao[[1;;d,1;;d]],
"\[Mu]Uncertainty"->\[Mu]unc,
"\[CapitalSigma]Uncertainty"->\[CapitalSigma]unc,
"UnbiasedGuess"->OptionValue["UnbiasedGuess"]
|>,
"Distribution" (* discards uncertainty covariances *),
MultinormalDistribution[MapThread[Around,{\[Mu]result,\[Mu]unc},1],MapThread[Around,{\[CapitalSigma]result,\[CapitalSigma]unc},2]],
_ (* give association as default *),
Message[NMultinormalAnalyze::output,OptionValue["Output"]];
<|
"Parameters"->paramnames,
"Estimates"->estimates,
"FisherInformation"->\[ScriptCapitalI],
"CovarianceMatrix"->CramerRao,
"\[Mu]"->\[Mu]result,"\[CapitalSigma]"->\[CapitalSigma]result,
"\[Mu]Covariance"->CramerRao[[1;;d,1;;d]],
"\[Mu]Uncertainty"->\[Mu]unc,
"\[CapitalSigma]Uncertainty"->\[CapitalSigma]unc,
"UnbiasedGuess"->OptionValue["UnbiasedGuess"]
|>
]
];


Options[NMultinormalAnalyze]={Verbose->False,"UnbiasedGuess"->True,"Constrain\[CapitalSigma]"->True,"FitDiagnostics"->False,ImageSize->400,PlotPoints->Automatic,MaxRecursion->Automatic,"Output"->"Association"};


NNormalAnalyze[data_List,options:OptionsPattern[NMultinormalAnalyze]]:=NMultinormalAnalyze[MapAt[{#}&,{All,1}]@MapAt[{{#^2}}&,{All,2}]@data,options]


SeparateMultinormal[\[FormalA]_MultinormalDistribution]:=(If[Not@DiagonalMatrixQ[Covariance[\[FormalA]]],Message[SeparateMultinormal::covariance]];Inner[NormalDistribution,Mean[\[FormalA]],StandardDeviation[\[FormalA]],Times])


NIndepNormalAnalyze[data_List,options:OptionsPattern[NMultinormalAnalyze]]:=Module[{d,n,results},
(* Do NMultinormalAnalyze discarding correlations*)
If[Depth[data]=!=5,Message[NMultinormalAnalyze::depth];Return[$Failed]];
If[Length[#]===1,d=First[#],
Message[NMultinormalAnalyze::dimension];Return[$Failed]
]&[Union@Flatten@Map[Dimensions,data,{2}]];
n=Length[data];

results=Table[NNormalAnalyze[{#[[1,\[FormalI]]],Sqrt[#[[2,\[FormalI],\[FormalI]]]]}&/@data,options],{\[FormalI],d}];

<|
"Parameters"->Flatten[Table[{Subscript["\[Mu]",\[FormalI]],Subscript[If[OptionValue["UnbiasedGuess"],"\[CapitalSigma]s","\[CapitalSigma]"],Row[{\[FormalI],\[FormalI]}," "]]},{\[FormalI],d}]],
"Estimates"->Flatten[Lookup[#,"Estimates"]&/@results],
"FisherInformation"->BlockDiagonalMatrix[Lookup[#,"FisherInformation"]&/@results],
"CovarianceMatrix"->BlockDiagonalMatrix[Lookup[#,"CovarianceMatrix"]&/@results],
"\[Mu]"->Flatten[Lookup[#,"\[Mu]"]&/@results],
"\[CapitalSigma]"->BlockDiagonalMatrix[Lookup[#,"\[CapitalSigma]"]&/@results],
"\[Mu]Covariance"->BlockDiagonalMatrix[Lookup[#,"\[Mu]Covariance"]&/@results],
"\[Mu]Uncertainty"->Flatten[Lookup[#,"\[Mu]Uncertainty"]&/@results],
"\[CapitalSigma]Uncertainty"->BlockDiagonalMatrix[Lookup[#,"\[CapitalSigma]Uncertainty"]&/@results],
"UnbiasedGuess"->OptionValue["UnbiasedGuess"]
|>
];


(* ::Section:: *)
(*End*)


End[];


EndPackage[];
